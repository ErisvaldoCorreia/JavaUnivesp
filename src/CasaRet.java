/*
 * Programa criado em conjunto com as video aulas
 * do curso de Programação de Computadores da Univesp
 */

class CasaRet {

  public static void main(String[] args) {

    // declarando as variaveis
    int areaq;
    int areas;
    int areat;

    // imprime uma mensagem sobre o que o programa faz
    System.out.println("Programa para calculo da area de uma casa!");

    // calculos
    areas = 10 * 10;
    areaq = 5 * 7;
    areat = (2 * areaq) + areas;

    // exibindo resultados
    System.out.println("A area da sala é: " + areas);
    System.out.println("A area do quarto é: " + areaq);
    System.out.println("A area do banheiro é: " + areaq);
    System.out.println("A area total da casa é: " + areat);

  }
}
